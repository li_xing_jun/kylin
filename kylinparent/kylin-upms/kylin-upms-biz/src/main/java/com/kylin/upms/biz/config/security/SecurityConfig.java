/*
package com.kylin.upms.biz.config.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
    @Override
    protected void configure(HttpSecurity http) throws Exception {
            */
/*authorizeRequests:认证请求
                        antMatchers：排除url
                        permitAll：所有请求
                        formLogin：什么登陆  这里是  表单登陆
                        loginPage：登录页  如果未登录跳转那个登录页
                        loginProcessingUrl：认证是那个url地址
                    * *//*


        http.authorizeRequests().anyRequest().authenticated().and().formLogin().permitAll();
    }
}
*/
