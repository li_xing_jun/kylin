package com.kylin.upms.biz.web;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.kylin.upms.biz.dto.UserDto;
import com.kylin.upms.biz.entity.User;
import com.kylin.upms.biz.service.IUserService;
import com.kylin.upms.biz.vo.ResEntity;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Mht
 * @since 2019-09-18
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    IUserService iUserService;
    //分页查询
    @ApiOperation("根据查询条件分页查询用户列表")
    @RequestMapping(method = RequestMethod.GET,value = "/page")
    public ResEntity get(UserDto userDto){
        //封装page  使用mybatis工具中的page对象
        Page<User> page=new Page<User>(userDto.getPageNum(),userDto.getPageSize());

        User user=new User();
        BeanUtils.copyProperties(userDto,user);
        EntityWrapper entityWrapper=new EntityWrapper(user);
        entityWrapper.like("username",user.getUsername());
        Page page1 = iUserService.selectPage(page, entityWrapper);
        return ResEntity.ok(page1);
    }

    //新增
    @RequestMapping(method = RequestMethod.POST)
    public ResEntity add(UserDto userDto){
        return null;
    }

    //修改
    @RequestMapping(method = RequestMethod.PUT)
    public ResEntity update(UserDto userDto){
        return null;
    }

    //删除
    @RequestMapping(method = RequestMethod.DELETE)
    public ResEntity delete(UserDto userDto){
        return null;
    }

    //根据id查询
    @RequestMapping(value = "/getByid",method = RequestMethod.GET)
    public ResEntity get(Integer id){
        return null;
    }

}
