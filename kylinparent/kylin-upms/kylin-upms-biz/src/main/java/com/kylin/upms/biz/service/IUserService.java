package com.kylin.upms.biz.service;

import com.kylin.upms.biz.entity.User;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Mht
 * @since 2019-09-18
 */
public interface IUserService extends IService<User> {

}
