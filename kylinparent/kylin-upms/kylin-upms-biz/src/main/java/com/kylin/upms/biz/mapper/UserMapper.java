package com.kylin.upms.biz.mapper;

import com.kylin.upms.biz.entity.Role;
import com.kylin.upms.biz.entity.User;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Mht
 * @since 2019-09-18
 */
public interface UserMapper extends BaseMapper<User> {
    public List<Role> getRoleByUserName(String username);
}
