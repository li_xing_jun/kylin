package com.kylin.upms.biz.web;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Mht
 * @since 2019-09-18
 */
@Controller
@RequestMapping("/employee")
public class EmployeeController {

}
